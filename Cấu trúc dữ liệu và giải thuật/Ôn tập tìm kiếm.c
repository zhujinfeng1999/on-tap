#define _CRT_SECURE_NO_WARNINGS

/*
	Tìm kiếm sinh viên theo mã số bằng 2 thuật toán tìm kiếm tuần tự và tìm kiếm nhị phân
*/

#include <stdio.h>
#include <string.h>
#define MAX 50

typedef struct {
	char MSSV[11];
	char HoTen[50];
	float DTB;
} SinhVien;

void NhapSoLuong(int *SoLuong) {
	do {
		printf("Nhap so luong sinh vien: ");
		scanf("%d", SoLuong);
	} while (*SoLuong <= 0 && *SoLuong > MAX);
}

void NhapSinhVien(SinhVien *SV) {
	rewind(stdin);
	printf("\nNhap ma so: ");
	gets(SV->MSSV);
	printf("Nhap ho ten: ");
	gets(SV->HoTen);
	printf("Nhap DTB: ");
	scanf("%f", &SV->DTB);
}

void XuatSinhVien(SinhVien SV) {
	printf("\nMa so:\t%s\n", SV.MSSV);
	printf("Ho ten:\t%s\n", SV.HoTen);
	printf("DTB:\t%.2f\n", SV.DTB);
}

void NhapDanhSachSinhVien(SinhVien SV[], int SoLuong) {
	for (int i = 0; i < SoLuong; i++) {
		NhapSinhVien(&SV[i]);
	}
}

void XuatDanhSachSinhVien(SinhVien SV[], int SoLuong) {
	for (int i = 0; i < SoLuong; i++) {
		XuatSinhVien(SV[i]);
	}
}

int TimKiemTuanTu(SinhVien SV[], int SoLuong, char MSSV[]) {
	for (int i = 0; i < SoLuong; i++) {
		if (strcmp(SV[i].MSSV, MSSV) == 0) return i;
	}
	return -1;
}

void HoanVi(SinhVien *A, SinhVien *B) {
	SinhVien C = *A;
	*A = *B;
	*B = C;
}

void SapXepTrucTiep(SinhVien SV[], int SoLuong) {
	for (int i = 0; i < SoLuong - 1; i++) {
		for (int j = i + 1; j < SoLuong; j++) {
			if (strcmp(SV[i].MSSV, SV[j].MSSV) > 0) {
				HoanVi(&SV[i], &SV[j]);
			}
		}
	}
}

int TimKiemNhiPhan(SinhVien SV[], int SoLuong, char MSSV[]) {
	SapXepTrucTiep(SV, SoLuong);

	int left = 0, right = SoLuong - 1;

	while (left <= right) {
		int mid = (left + right) / 2;
		
		if (strcmp(SV[mid].MSSV, MSSV) == 0) return mid;

		if (strcmp(MSSV, SV[mid].MSSV) < 0) {
			right = mid - 1;
		}
		else {
			left = mid + 1;
		}
	}
	return -1;
}

int main() {
	SinhVien SV[MAX];
	int SoLuong;

	NhapSoLuong(&SoLuong);

	NhapDanhSachSinhVien(SV, SoLuong);
	
	XuatDanhSachSinhVien(SV, SoLuong);

	puts("\nTim kiem tuan tu sinh vien co ma so la 1\n");
	int ViTri = TimKiemTuanTu(SV, SoLuong, "1");
	XuatSinhVien(SV[ViTri]);

	puts("\nTim kiem nhi phan sinh vien co ma so la 2\n");
	ViTri = TimKiemTuanTu(SV, SoLuong, "2");
	XuatSinhVien(SV[ViTri]);

	return 0;
}